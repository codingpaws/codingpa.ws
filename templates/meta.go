package templates

import (
	"fmt"
	"strings"
	"time"

	"codingpa.ws/internal/util"
	"github.com/a-h/templ"
)

type Meta struct {
	Path              string
	Title             string
	Description       string
	ImageUrl          string
	Version           string
	Content           string
	ReadTimeInMinutes int
	Date              *time.Time
	IsHome            bool
}

func (self Meta) is404() bool {
	return self.Path == "404"
}

func (self Meta) twitterCardType() string {
	if self.ImageUrl != "" {
		return "summary_large_image"
	}

	return "summary"
}

func (self Meta) seoCanonicalURL() string {
	canonicalPath := self.Path
	canonicalPath = strings.TrimSuffix(canonicalPath, "index")
	canonicalPath = strings.TrimSuffix(canonicalPath, "/")
	return "https://codingpa.ws/" + canonicalPath
}

func (self Meta) seoImageURL() string {
	if self.ImageUrl != "" {
		return self.ImageUrl
	}

	return "https://codingpa.ws/og-image.jpg"
}

func (self Meta) pawsScriptURL() string {
	base := "/js/paws.js?v="
	if !util.IsProduction() {
		base = "/js/paws.dev.js?v="
	}
	return base + self.Version
}

func (self Meta) cssURL() string {
	return "/css/blog.css?v=" + self.Version
}

func (self Meta) seoDescription() string {
	if self.Description == "" {
		return "real dog with coding paws"
	}

	return self.Description
}

func (self Meta) seoTitle() string {
	title := self.Title
	if title == "" {
		title = "codingpaws"
	} else {
		title += " · codingpaws"
	}
	return title
}

func (self Meta) sourceURL() templ.SafeURL {
	return templ.URL(fmt.Sprintf("https://gitlab.com/codingpaws/codingpa.ws/-/tree/main/pages/%s.md", self.Path))
}
