package main

import (
	"fmt"
	"io/fs"
	"log"
	"mime"
	"os"
	"path/filepath"
	"time"

	"codingpa.ws/app"
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/css"
	"github.com/tdewolff/minify/html"
	"github.com/tdewolff/minify/js"
	"github.com/tdewolff/minify/xml"
)

func main() {
	if err := app.Run(); err != nil {
		log.Fatalln(err)
	}

	start := time.Now()

	const path = "public"

	m := minify.New()
	m.AddFunc("text/css", css.Minify)
	m.AddFunc("text/html", html.Minify)
	m.AddFunc("text/javascript", js.Minify)
	m.AddFunc("text/xml", xml.Minify)

	fmt.Println("minifing...")

	err := filepath.Walk(path, func(path string, info fs.FileInfo, err error) error {
		if err != nil || info.IsDir() {
			return err
		}
		typ := mime.TypeByExtension(filepath.Ext(path))
		if _, _, fn := m.Match(typ); fn == nil {
			return nil
		}
		b, err := os.ReadFile(path)
		if err != nil {
			return err
		}
		str, err := m.String(typ, string(b))
		if err != nil {
			return fmt.Errorf("minifying %s: %w", path, err)
		}
		err = os.WriteFile(path, []byte(str), info.Mode())
		if err != nil {
			return err
		}
		fmt.Printf("  %s (%0.2f%%)\n", path, (float64(len(str)-len(b))/float64(len(b)))*100)
		return nil
	})
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("minified in", time.Since(start))
}
