module codingpa.ws

go 1.23.2

require github.com/yuin/goldmark v1.7.8

require (
	github.com/BurntSushi/toml v1.4.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/tdewolff/minify v2.3.6+incompatible // indirect
	github.com/tdewolff/parse v2.3.4+incompatible // indirect
	golang.org/x/sys v0.27.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/a-h/templ v0.2.793
	github.com/fsnotify/fsnotify v1.8.0
	github.com/stretchr/testify v1.10.0
	go.abhg.dev/goldmark/frontmatter v0.2.0
	golang.org/x/sync v0.9.0
	gopkg.in/yaml.v2 v2.4.0
)
