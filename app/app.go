package app

import (
	"context"
	"errors"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"codingpa.ws/internal/files"
	"codingpa.ws/internal/md"
	"codingpa.ws/internal/util"
	"codingpa.ws/templates"
)

type TemplateInfo struct {
	Meta      md.Meta `json:"meta"`
	WordCount int
	Markdown  string `json:"markdown"`
}

const crossSymbol = "\x1b[31m✘\x1b[0m"
const checkSymbol = "\x1b[32m✓\x1b[0m"
const homePath = "pages/index.md"

var gitRevision, gitRevError = util.GitRevisionForCache()
var metas []templates.LinkedMeta

func Run() (err error) {
	if gitRevError != nil {
		return fmt.Errorf("getting git revision: %w", gitRevError)
	}

	firstStart := time.Now()
	fmt.Println("\x1b[33;1mcodingpa.ws\x1b[0m 🐾 static site generator")
	fmt.Println()

	contents, err := files.Get()
	if err != nil {
		return
	}

	errCount := 0

	var home *TemplateInfo

	for path, content := range contents {
		start := time.Now()
		path = strings.Replace(path, "/_index/", "/", 1)
		info, err := processFile(path, content)

		if err != nil {
			return fmt.Errorf("%s: %w", path, err)
		}

		if path == homePath {
			home = &info
			continue
		}

		if newPath, err := writeFile(path, info); err != nil {
			fmt.Printf("%s failed \x1b[34;1m%s\x1b[0m:\n  %s", crossSymbol, path, err)
			errCount++
		} else {
			addRssEntry(info.Meta, newPath)
			metas = append(metas, templates.LinkedMeta{Meta: info.Meta, Link: strings.Replace(newPath, "public", "", 1)})
			fmt.Printf("%s compiled \x1b[34;1m%s\x1b[0m to \x1b[34;1m%s\x1b[0m \x1b[2m[%s]\x1b[0m\n", checkSymbol, path, newPath, time.Since(start))
		}
	}

	if home != nil {
		sort.Slice(metas, func(i, j int) bool {
			a := metas[i].Date
			b := metas[j].Date

			if a == nil || b == nil {
				return true
			}

			return a.Unix() > b.Unix()
		})
		_, err := writeFile(homePath, *home)

		if err != nil {
			return err
		}
	}

	err = generateSitemap()
	if err != nil {
		return fmt.Errorf("generating sitemap: %w", err)
	}
	err = generateRssFeed()
	if err != nil {
		return fmt.Errorf("generating rss feed: %w", err)
	}

	var symbol string

	if errCount > 0 {
		symbol = crossSymbol
		err = errors.New("compiling one or more failed")
	} else {
		symbol = checkSymbol
	}

	fmt.Printf("\n%s done \x1b[2m[%s]\x1b[0m\n", symbol, time.Since(firstStart))

	return err
}

func processFile(path string, content []byte) (info TemplateInfo, err error) {
	markdown, meta, err := md.GenerateHtmlAndMeta(path, content)

	if err != nil {
		return
	}

	return TemplateInfo{
		Meta:      *meta,
		Markdown:  markdown,
		WordCount: util.CountWords(content),
	}, nil
}

func writeFile(path string, info TemplateInfo) (newPath string, err error) {
	newPath = files.ToPublicPath(path)

	os.MkdirAll(filepath.Dir(newPath), 0775)
	if err != nil {
		return
	}

	file, err := os.Create(newPath + ".html")
	if err != nil {
		return
	}
	defer file.Close()

	templ, err := templates.Render(templates.Meta{
		Path:              strings.TrimPrefix(newPath, "public/"),
		Title:             info.Meta.Title,
		Description:       info.Meta.Description,
		ImageUrl:          info.Meta.ImageURL,
		Version:           gitRevision,
		Content:           info.Markdown,
		Date:              info.Meta.Date,
		ReadTimeInMinutes: int(math.Round(math.Max(float64(info.WordCount)/220.0, 1.0))),
		IsHome:            path == homePath, // TODO
	}, metas)
	if err != nil {
		return
	}

	err = templ.Render(context.TODO(), file)

	return
}
