from docker.io/library/golang:1.23.2-alpine as builder

arg CI_COMMIT_BRANCH

env PATH=$PATH:/usr/local/go/bin
workdir /app
copy . .
run apk add git
run ./build.sh
run go build ./cmd/serve

from docker.io/library/alpine:latest
workdir /public
copy --from=builder /app/public /public
copy --from=builder /app/serve /bin/serve
entrypoint ["serve"]
