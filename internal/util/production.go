package util

import "os"

func IsProduction() bool {
	return os.Getenv("CI_COMMIT_BRANCH") == "main"
}
