package util_test

import (
	"testing"

	"codingpa.ws/internal/util"
	"github.com/stretchr/testify/require"
)

func TestWordCount(t *testing.T) {
	testCases := []struct {
		str   string
		words int
	}{
		{
			str:   "",
			words: 0,
		},
		{
			str:   "## Doggo ipsum *borkf* heckin angery woofer mlem `heck`, puggorino.",
			words: 10,
		},
		{
			str:   " \t \n \n",
			words: 0,
		},
		{
			str:   "1 2 # - $ ~ <br>",
			words: 7,
		},
	}
	for _, tC := range testCases {
		t.Run("example: "+tC.str, func(t *testing.T) {
			require.Equal(t, tC.words, util.CountWords([]byte(tC.str)))
		})
	}
}
