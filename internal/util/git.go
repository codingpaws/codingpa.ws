package util

import (
	"os/exec"
	"strings"
)

func GitRevisionForCache() (string, error) {
	cmd := exec.Command("git", "rev-parse", "HEAD")

	b, err := cmd.Output()
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(string(b))[8:16], nil
}
