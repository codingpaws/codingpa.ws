package util_test

import (
	"os"
	"testing"

	"codingpa.ws/internal/util"
	"github.com/stretchr/testify/require"
)

func TestIsProduction(t *testing.T) {
	env, hasEnv := os.LookupEnv("CI_COMMIT_BRANCH")
	t.Cleanup(func() {
		if hasEnv {
			os.Setenv(env, env)
		}
	})

	t.Run("production", func(t *testing.T) {
		os.Setenv("CI_COMMIT_BRANCH", "main")

		require.True(t, util.IsProduction())
	})

	t.Run("branch", func(t *testing.T) {
		os.Setenv("CI_COMMIT_BRANCH", "kevslashnull-patch-0")

		require.False(t, util.IsProduction())
	})

	t.Run("local", func(t *testing.T) {
		os.Unsetenv("CI_COMMIT_BRANCH")

		require.False(t, util.IsProduction())
	})
}
