package util

import (
	"bufio"
	"bytes"
)

func CountWords(content []byte) (count int) {
	scanner := bufio.NewScanner(bytes.NewReader(content))
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		count++
	}
	return
}
