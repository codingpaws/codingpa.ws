package files_test

import (
	"testing"

	"codingpa.ws/internal/files"
	"github.com/stretchr/testify/require"
)

func TestToPublicPath(t *testing.T) {
	publicPath := files.ToPublicPath("pages/post/example-post.md")
	require.Equal(t, "public/post/example-post", publicPath)
}
