package files

import (
	"fmt"
	"strings"
)

func ToPublicPath(sourcePath string) string {
	sourcePath = strings.TrimSuffix(sourcePath, ".md")
	sourcePath = strings.TrimPrefix(sourcePath, "pages/")

	return fmt.Sprintf("public/%s", sourcePath)
}
