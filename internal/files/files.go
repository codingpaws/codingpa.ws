package files

import (
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

const basepath = "pages"

func Get() (map[string][]byte, error) {
	paths, err := listMarkdownPaths()

	if err != nil {
		return nil, err
	}

	contents := map[string][]byte{}

	for _, path := range paths {
		var content []byte

		if content, err = os.ReadFile(path); err != nil {
			return nil, err
		}

		contents[path] = content
	}

	return contents, nil
}

func listMarkdownPaths() ([]string, error) {
	paths := []string{}

	err := filepath.Walk(basepath, func(path string, info fs.FileInfo, err error) error {
		if strings.HasPrefix(info.Name(), "_") && os.Getenv("ENV") != "development" {
			return nil
		}

		if strings.Contains(path, "/unpublished/") {
			return nil
		}

		if strings.HasSuffix(info.Name(), ".md") {
			paths = append(paths, path)
		}

		return nil
	})

	return paths, err
}
