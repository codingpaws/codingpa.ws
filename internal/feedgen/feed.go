package feedgen

import (
	"bytes"
	"fmt"
	"html/template"
	"sort"
	"strings"
	"time"

	"codingpa.ws/internal/md"
)

const publication = `<news:publication>
<news:name>codingpaws</news:name>
<news:language>en</news:language>
</news:publication>`

type info struct {
	Entries *list
	Date    time.Time
}

func (info) Year() string      { return time.Now().Format("2006") }
func (e info) RssDate() string { return e.Date.Format(time.RFC1123Z) }

type entry struct {
	Title string
	URL   string
	Meta  md.Meta
	Date  time.Time
}

func (e entry) RssDate() string { return e.Date.Format(time.RFC1123Z) }

func (lst *list) ToSitemap() []byte {
	parts := []string{
		`<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9">`,
	}

	sort.Sort(lst)

	for _, value := range *lst {
		parts = append(parts, fmt.Sprintf(`<url>
			<loc>%s</loc>
			<news:news>
				<news:title>%s</news:title>
				<news:publication_date>%s</news:publication_date>
				%s
			</news:news>
		</url>`, value.URL, value.Title, value.Date.Format("2006-01-02T15:04:05Z"), publication))
	}
	parts = append(parts, "</urlset>")
	parts = strings.Split(strings.Join(parts, "\n"), "\n")

	for i, value := range parts {
		parts[i] = strings.Trim(value, " \t")
	}

	return []byte(strings.Join(parts, "\n"))
}

func (lst *list) ToRss(path string) (res []byte, err error) {
	var html *template.Template

	if html, err = template.ParseFiles("templates/rss.html"); err != nil {
		return nil, err
	}

	buf := bytes.NewBuffer([]byte{})

	buf.WriteString(`<?xml version="1.0" encoding="UTF-8"?>`)

	err = html.Execute(buf, info{
		Entries: lst,
		Date:    time.Now(),
	})

	return buf.Bytes(), err
}
