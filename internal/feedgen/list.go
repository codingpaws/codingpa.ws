package feedgen

import (
	"time"

	"codingpa.ws/internal/md"
)

type list []entry

func (a list) Len() int           { return len(a) }
func (a list) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a list) Less(i, j int) bool { return a[i].Date.Sub(a[j].Date) > 0 }

func NewList() *list {
	return &list{}
}

func (lst *list) Add(title string, url string, meta md.Meta, date time.Time) {
	*lst = append(*lst, entry{title, url, meta, date})
}
