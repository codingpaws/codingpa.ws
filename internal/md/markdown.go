package md

import (
	"bytes"
	"fmt"
	"image"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	_ "image/jpeg"
	_ "image/png"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer/html"
	"github.com/yuin/goldmark/text"
	"github.com/yuin/goldmark/util"
	"go.abhg.dev/goldmark/frontmatter"
)

const DateFormat = "2006-01-02"

type Meta struct {
	Title       string     `yaml:"title"`
	Description string     `yaml:"description"`
	Date        *time.Time `yaml:"date"`
	ImageURL    string     `yaml:"image"`
	RSS         bool       `yaml:"rss"`
	Home        bool       `yaml:"home"`
}

func GenerateHtmlAndMeta(path string, contents []byte) (string, *Meta, error) {
	markdown := goldmark.New(
		goldmark.WithExtensions(
			new(frontmatter.Extender),
			extension.GFM,
			extension.Footnote,
		),
		goldmark.WithRendererOptions(
			html.WithUnsafe(),
		),
		goldmark.WithParserOptions(
			parser.WithInlineParsers(
				util.Prioritized(linkParser{path, parser.NewLinkParser(), extension.NewFootnoteParser()}, 101),
			),
		),
	)

	var buf bytes.Buffer
	context := parser.NewContext()
	if err := markdown.Convert(contents, &buf, parser.WithContext(context)); err != nil {
		return "", nil, err
	}

	var meta *Meta
	frontmatter := frontmatter.Get(context)
	err := frontmatter.Decode(&meta)
	if err != nil {
		return "", nil, fmt.Errorf("decoding frontmatter: %w", err)
	}
	if meta == nil {
		meta = new(Meta)
	}

	html := strings.Trim(buf.String(), " \n")

	return html, meta, nil
}

type linkParser struct {
	path           string
	linkParser     parser.InlineParser
	footnoteParser parser.InlineParser
}

var replaceMdRegex = regexp.MustCompile(`\.md$`)

func (p linkParser) Parse(parent ast.Node, block text.Reader, pc parser.Context) ast.Node {
	node := p.linkParser.Parse(parent, block, pc)

	if node != nil && node.Kind() == ast.KindLink {
		link := node.(*ast.Link)

		url, err := url.Parse(string(link.Destination))

		if err != nil {
			log.Fatalf("parsing url in %s: %s", p.path, err)
		}

		if url.Host == "" && strings.HasSuffix(url.Path, ".md") {
			dir := filepath.Dir(strings.Replace(p.path, "pages", "", 1))
			if !url.IsAbs() {
				url.Path = filepath.Join(dir, url.Path)
			}
			link.Destination = []byte(replaceMdRegex.ReplaceAllString(url.Path, ""))
		}
	}

	if node, ok := node.(*ast.Image); ok {
		dest := string(node.Destination)
		if strings.HasPrefix(dest, "/") {
			path := filepath.Join("./static", dest)
			f, err := os.Open(path)
			if err != nil {
				log.Fatalf("parsing image in %s: %s", p.path, err)
			}
			defer f.Close()
			metadata, _, err := image.DecodeConfig(f)
			if err != nil {
				log.Fatalf("parsing image in %s: %s", p.path, err)
			}

			node.SetAttributeString("width", []byte(fmt.Sprint(metadata.Width)))
			node.SetAttributeString("height", []byte(fmt.Sprint(metadata.Height)))
		}
	}

	if node == nil {
		node = p.footnoteParser.Parse(parent, block, pc)
	}

	return node
}

func (p linkParser) Trigger() []byte {
	return []byte{'!', '[', ']'}
}
