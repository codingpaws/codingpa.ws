package links

import (
	"os"
	"slices"

	"codingpa.ws/internal/util"
	"github.com/a-h/templ"
	"gopkg.in/yaml.v2"
)

type GlobalLink struct {
	Href           string `yaml:"href"`
	Title          string `yaml:"title"`
	Target         string `yaml:"target"`
	ProductionOnly bool   `yaml:"production_only"`
}

func (self GlobalLink) SafeHref() templ.SafeURL {
	return templ.URL(self.Href)
}

func Get() (links []GlobalLink, err error) {
	f, err := os.Open("pages/links.yml")
	if err != nil {
		return
	}
	defer f.Close()

	err = yaml.NewDecoder(f).Decode(&links)
	if err != nil {
		return
	}
	isProduction := util.IsProduction()
	links = slices.DeleteFunc(links, func(link GlobalLink) bool {
		return link.ProductionOnly && !isProduction
	})
	return
}
