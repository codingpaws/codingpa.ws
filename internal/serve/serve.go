package serve

import (
	"bufio"
	"io"
	"log"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"slices"
	"strings"
)

type server struct {
	cwd string
}

func Start(dir string) error {
	server := &server{cwd: dir}

	return http.ListenAndServe(":3000", http.HandlerFunc(server.handle))
}

func (server *server) handle(w http.ResponseWriter, r *http.Request) {
	writer := bufio.NewWriter(w)
	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.WriteHeader(http.StatusMethodNotAllowed)
		_, _ = writer.WriteString("method not allowed")
		return
	}

	path := strings.Trim(r.URL.Path, "/")
	path = filepath.Clean(path)

	if path == "." {
		server.pushOutFile(w, r, "index.html")
		return
	}

	if server.exist(path) {
		server.pushOutFile(w, r, path)
		return
	}

	if server.exist(path + ".html") {
		server.pushOutFile(w, r, path+".html")
		return
	}

	if server.exist(path + "/index.html") {
		server.pushOutFile(w, r, path+"/index.html")
		return
	}

	server.send404(w, r)
}

func (server *server) exist(path string) bool {
	path = filepath.Join(server.cwd, path)

	_, err := os.Stat(path)
	return err == nil
}

func (server *server) pushOutFile(w http.ResponseWriter, r *http.Request, path string) {
	server.pushOutFileWithStatusCode(w, r, path, http.StatusOK)
}

func (server *server) send404(w http.ResponseWriter, r *http.Request) {
	server.pushOutFileWithStatusCode(w, r, "404.html", http.StatusNotFound)
}

func (server *server) pushOutFileWithStatusCode(w http.ResponseWriter, r *http.Request, path string, statusCode int) {
	path = filepath.Join(server.cwd, path)

	f, err := os.Open(path)
	if err != nil {
		log.Println(err)
		server.send404(w, r)
		return
	}
	defer f.Close()

	ext := filepath.Ext(path)
	w.Header().Set("content-type", mime.TypeByExtension(ext))
	if shouldCacheExtension(ext) {
		w.Header().Set("cache-control", "public, max-age=0, must-revalidate")
	}
	w.WriteHeader(statusCode)

	if r.Method == http.MethodHead {
		return
	}

	_, _ = io.Copy(w, f)
}

func shouldCacheExtension(ext string) bool {
	cachableExtension := []string{
		".html",
		".xml",
		".js",
		".css",
		".ico",
		".woff2",
	}

	return slices.Contains(cachableExtension, ext)
}
