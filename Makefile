.PHONY: vale
vale:
	@vale $$(find -name '*.md' -not -name '_*' -not -name '.gitignore')