package main_test

import (
	"io/fs"
	"os"
	"path/filepath"
	"testing"

	"codingpa.ws/app"
	"github.com/stretchr/testify/require"
)

func TestAppRun(t *testing.T) {
	err := os.RemoveAll("./public")
	require.NoError(t, err)

	err = app.Run()
	require.NoError(t, err)

	b, err := os.ReadFile("./public/index.html")
	require.NoError(t, err)

	require.Contains(t, string(b), "real dog with coding paws")

	assets := findGeneratedAssets(t)

	require.Contains(t, assets, "public/404.html")
	require.Contains(t, assets, "public/sitemap.xml")
	require.Contains(t, assets, "public/post/dont-use-the-double-tilde-in-js.html")
}

func findGeneratedAssets(t *testing.T) (assets []string) {
	err := filepath.Walk("public", func(path string, info fs.FileInfo, err error) error {
		if !info.IsDir() {
			assets = append(assets, path)
		}
		return err
	})
	require.NoError(t, err)
	return
}
