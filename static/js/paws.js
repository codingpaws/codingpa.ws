let loaded = false;

document.addEventListener("DOMContentLoaded", () => {
  if (loaded) return;
  loaded = true;
  const article = document.querySelector("article");
  const paragraphs = article.querySelectorAll("p:last-of-type");
  const lastParagraph = paragraphs[paragraphs.length - 1];

  const pawTarget = article.querySelector('.js-paw');
  if (pawTarget) {
    pawTarget.textContent = "🐾";
  } else if (lastParagraph) {
    const { childNodes } = lastParagraph;
    const node = childNodes[childNodes.length - 1];
    node.textContent += " 🐾";
  }

  if (window.location.hostname === "localhost") {
    startDevHotReloadWatchdog();
  }
});

async function startDevHotReloadWatchdog() {
  const lastCheck = Date.now();
  const waitAfterReload = 2_000;

  const etag = await fetchCurrentEtag();

  const intervalId = setInterval(async () => {
    if (Date.now() - lastCheck < waitAfterReload) {
      return;
    }

    let newEtag;
    try {
      newEtag = await fetchCurrentEtag();
      if (!newEtag) return;
    } catch (error) {
      return;
    }

    if (newEtag !== etag) {
      console.info("Found new version, reloading...");
      clearInterval(intervalId);
      setTimeout(() => {
        window.location.reload();
      }, 250);
    }
  }, 750);
}

async function fetchCurrentEtag() {
  return fetch("/.hot-reload").then((r) => {
    if (!r.ok) throw Error(`${r.status} must be 200`);
    return r.text();
  });
}
