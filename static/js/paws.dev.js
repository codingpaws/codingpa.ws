const script = document.createElement('script');
script.src = "/js/paws.js";
script.onload = () => document.dispatchEvent(new CustomEvent('DOMContentLoaded'));
document.body.appendChild(script)

document.addEventListener("DOMContentLoaded", () => {
  if (window.location.hostname === "localhost") {
    startDevHotReloadWatchdog();
  }
});

async function startDevHotReloadWatchdog() {
  const lastCheck = Date.now();
  const waitAfterReload = 2_000;

  const etag = await fetchCurrentEtag();

  const intervalId = setInterval(async () => {
    if (Date.now() - lastCheck < waitAfterReload) {
      return;
    }

    let newEtag;
    try {
      newEtag = await fetchCurrentEtag();
      if (!newEtag) return;
    } catch (error) {
      return;
    }

    if (newEtag !== etag) {
      console.info("Found new version, reloading...");
      clearInterval(intervalId);
      setTimeout(() => {
        window.location.reload();
      }, 250);
    }
  }, 750);
}

async function fetchCurrentEtag() {
  return fetch("/.hot-reload").then((r) => {
    if (!r.ok) throw Error(`${r.status} must be 200`);
    return r.text();
  });
}
