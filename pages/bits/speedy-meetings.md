---
title: Speedy meetings
description: Time is valuable, let’s avoid jumping from meeting to meeting.
date: 2022-04-29
---

Meetings, either in-person or virtually, are a relevant tool in business
communication. A single person might spend most of their weekly work time
in meetings. Asynchronous communication can reduce the number and
length of meetings. But, in some circumstances, meetings will be
scheduled one after the other.

For example, all the product managers I have worked with will spend some
days of the week jumping from meeting to meeting with little to no break
time in between meetings. If you have to take care of private-life tasks
while working remotely or you have to another conference room in an
office, you will be stressed out and late.

## A simple solution

You most likely use a scheduling tool like Outlook[^2] or Google
Calendar[^3] to arrange most of your meetings. These calendars often have
a **Speedy meetings** feature. It reduces the meeting time by 5 or 10
minutes.

For example, a 30-minute meeting at 9:30 will last until am 9:55 am. And
a 60-minute one at 2:00 pm will end at 2:50 pm.

By adding these few minutes between meetings, you’re respectful of your
colleagues’ time. You give them time to handle private things, go to the
toilet, meditate, find the meeting room, and more![^1]

And don’t worry about the fewer minutes in meetings. All participants
will be considerably more focused and relaxed. If everyone has the
headspace for the current meeting, its usefulness and productivity will
be noticeably better.

[^1]: [“Start on time, end on time”](https://about.gitlab.com/company/culture/all-remote/meetings/#3-start-on-time-end-on-time), GitLab Handbook
[^2]: [“Make all your events shorter automatically”](https://support.microsoft.com/en-us/office/make-all-your-events-shorter-automatically-8fa0fdcb-5eee-4e12-9452-8d59ee33f70a), Microsoft
[^3]: [“Work hacks from G Suite”](https://cloud.google.com/blog/products/g-suite/work-hacks-from-g-suite-how-to-host-more-effective-meetings), Google Cloud
