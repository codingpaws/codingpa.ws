---
title: No agendaless meetings
description: How do you have a meeting without an agenda? Simply, you don’t.
date: 2022-05-09
---

A few years ago, I was working in a team with many stakeholders. I had
both frequent scheduled and impromptu meetings. Someone would set up a
meeting in our calendar and invite all required people. The title and
often a short description would explain the reason for the meeting.

In objectively too many meetings, we’d find us digging down a rabbit
hole, hence losing track of the problem. We talked about this and decided
on a simple solution: we try to set up an agenda for all group meetings;
otherwise, we at least keep track of the discussion with meeting notes.

For every meeting I attend, I make sure that we
[have an agenda][glmeetings]. I’ll happily volunteer to set it up! My
favorite structure for meeting agendas is this simple one:

1. **Catch-up agenda** (only for recurring meeting)  
   We have to catch-up with the following topics from the last meeting.
   Topics that aren’t urgent act as an asynchronous update.
1. **Agenda**  
   Our main discussion points, added ideally before the meeting starts.
1. **Action items**  
   To-dos that are an immediate result of this meeting.

I’m still experimenting whether a catch-up agenda is useful. Because
ultimately, if you need to wait for a meeting to communicate your status,
your team has a major problem.

Each of these titles has sub-items for each discussion topic. A topic can
be a problem, question, or anything else you can talk about. The person
who adds the topic prefixes it with their name. `Kev:` in my case. If the
topic is only a status update, you prefix it with `[Async]` or `[FYI]`.

When beginning to talk about a topic, this gives room to the person to
talk about it without being talked over. This also helps people who are
[insecure or shy about raising their discussion topics][cherold]. And,
anyone who didn’t participate can quickly catch up and see all action
items.

[glmeetings]: https://about.gitlab.com/company/culture/all-remote/meetings/#4-all-meetings-must-have-an-agenda
[cherold]: https://thinkgrowth.org/stop-going-to-meetings-that-dont-have-agendas-31240f1b25c4
