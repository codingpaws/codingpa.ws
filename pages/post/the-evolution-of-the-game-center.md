---
title: Evolution of the SiegeGG Game Center
description: How we iterated a feature from usable to well-liked
image: https://codingpa.ws/img/evolution-of-the-game-center/image9.png
date: 2022-05-15
rss: true
home: true
---

SiegeGG is the place for Rainbow Six Siege esports coverage. We’ve grown
from a small Twitter page to an esports media company with millions of
page views per month. Last year, [Gfinity acquired SiegeGG][gfinacq] to
become part of the Gfinity Digital Media group.

[gfinacq]: https://www.gfinityplc.com/news/gfinity-acquires-siegegg-rainbow-six-siege-website-social-channel-acquisition-digital-media-gdm-investor-news-esports-uk/

But let’s rewind a bit in time. In June of 2020, we launched the [SiegeGG
Competitions][comps]. A SiegeGG competition is a league season, Major
event, or the yearly Six Invitational world championship. This release
was an important milestone for us and Rainbow Six Siege esports fans.

[comps]: https://siege.gg/competitions

Almost one year later, Ubisoft hosted the [Six Invitational 2021][si21].
As with the other Six Invitationals, we expected Ubisoft to host it in
February in Montreal, Canada. But due to the pandemic, it was delayed and
eventually hosted in Paris, France. After this 3-month delay, many people
were even more excited for competitive Siege on LAN. We, therefore, made
the Six Invitational page easy to find by spotlighting it as the
Highlighted Competition on the competition overview.

[si21]: https://siege.gg/competitions/297-six-invitational-2021

![Six Invitational 2021. LAN, Paris, France. May 11 to 23, 2021.](/img/evolution-of-the-game-center/image1.png)

Another year later, for the Six Invitational 2022, we envisioned
presenting fans a central place on SiegeGG to find the latest news, match
results, and stats about the Invitational. With only a week left before
the event would kick off, we designed and released the first iteration of
the SiegeGG Game Center on our home page.

![The first iteration of the game center. A few test articles on the left; the competition link and a prediction section on the right.](/img/evolution-of-the-game-center/image2.png)

After a few days, the Game Center had already gone through multiple small
iterations. Next up, in the second week of the event, the playoffs
started, where teams could now be eliminated. By then, the top of the
Game Center displayed the logos of all participating teams.

![A more advanced version of the game center. It has team logos, the match schedule, articles, the featured player card, and a section with links to the “By the numbers” articles.](/img/evolution-of-the-game-center/image3.png)

[Spencer][spencer] had the great idea of graying out a team’s logo once
the team is eliminated from the competition, leaving only the winner of
the Six Invitational to stand out after the grand finale. This had the
amazing effect we imagined it would have.

[spencer]: https://twitter.com/splekR6/

![All team logos except the one by TSM are grayed out and have a low opacity.](/img/evolution-of-the-game-center/image4.png)

There was only one problem. We hadn’t automated the elimination detection
yet. Outside my regular working hours, I would log into the production
server with my phone and manually gray out a team’s logo.

Now, back in the present, for the Six Major Charlotte 2022, the Game
Center takes up the entire width of the page from the beginning. Before
Ubisoft announced the group stage schedule, the Game Center displayed the
overall schedule of the week: a 3-days group stage, one pause day, and
the final 3-days playoffs.

![The game center with a schedule of the week.](/img/evolution-of-the-game-center/image5.jpg)

This time, it’ll also tie in with our new live articles. The [Charlotte
Major Hub][hub] live article will act as a news feed that fans can follow
along or use to recap what they might’ve missed.

[hub]: https://siege.gg/news/charlotte-major-hub

![A live article with placeholder text.](/img/evolution-of-the-game-center/image6.jpg)

Once Ubisoft published the group stage schedule, our stats team entered
all matches into the database. The Game Center immediately started
displaying the matches of the nearest play day (May 16th). Automation,
yeah!

![All matches of the first day of the major crammed in a single horizontal section.](/img/evolution-of-the-game-center/image7.png)

However, I didn’t expect 16 matches to be played in a single day, as we
had 8 per day at this year’s Six Invitational. Therefore, we grouped the
matches by where they are streamed. This not only fixed the layout but
also makes it clear where to watch a match. Meanwhile, the Game Center
started showing the team logos categorized by the team’s group too.

![The matches of the major are grouped in two rows based on where it is broadcasted, either the main or bravo Twitch channel.](/img/evolution-of-the-game-center/image8.png)

As [Elevate won’t be able to play during the Six Invitational
2022][elevate] due to visa issues, we had to remove their matches and
logo from the Game Center only a day later.

[elevate]: https://siege.gg/news/elevate-will-not-compete-at-charlotte-major-after-visa-application-denied

![Photo](/img/evolution-of-the-game-center/image9.png)

[Siege esports fans like the Game Center][twittergamecenter] and use it to stay up to date
about any ongoing Major competition. We’re looking forward to iterating
on it and continue making it even better.

[twittergamecenter]: https://twitter.com/Siege_GG/status/1524336603090817025
