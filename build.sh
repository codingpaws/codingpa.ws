#!/bin/sh
# We need this because `vercel dev` would 404 if dirs are deleted
mkdir ./public 2>/dev/null || echo > /dev/null
find ./public -type f -delete
cp -r static/* public/
go run .
