package main

import (
	"fmt"
	"log"
	"os"

	"codingpa.ws/internal/serve"
)

func main() {
	err := run()
	if err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	cwd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("getting workdir: %w", err)
	}

	return serve.Start(cwd)
}
